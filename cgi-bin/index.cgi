#!/usr/bin/awk -f
BEGIN {
if (ENVIRON["REQUEST_URI"] != ENVIRON["LOG_URI"]) {
	print "HTTP/1.1 404 Not Found\nConnection: close\n"
	exit
}
print "Content-type: text/html; charset=utf-8\nConnection: close\nCache-Control: max-age=10\n\
<!DOCTYPE html><html lang=uk><title>log</title><base target=\"_blank\">\
<style>td{padding:2px;vertical-align:top} td:nth-child(2){text-align:right} .gr{color:dimgray}</style><table>"
addlog(-86400)
addlog()
}{
if (FNR == 1)  # if first line
	print "<tr><td colspan=3>" (ARGIND==1?"":"<br>") "==&gt; " log_date[ARGIND] " &lt;==</td></tr>"
gsub(/&/, "\\&amp;")
gsub(/</, "\\&lt;")
gsub(/"/, "\\&quot;")
gsub(/https?:\/\/\S+/, "<a href=\"&\">&</a>")
print "<tr" (index($2,"--")?" class=gr":"") "><td>" $1 "</td><td>" $2 "</td><td>" ($1=$2="") $0 "</td></tr>"
} END { if (ARGC > 1) print "</table>" }
function addlog(offset) {
	ARGV[ARGC] = ENVIRON["LOG_DIR"] ( log_date[ARGC++] = strftime("%Y-%m-%d.log", systime()+offset) )
}
